package com.accenture.lbgpoc;

import java.text.SimpleDateFormat;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
@EnableBatchProcessing
public class LbgPocApplication implements CommandLineRunner {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private ApplicationContext context;

	public static void main(String[] args) {
		System.exit(SpringApplication.exit(SpringApplication.run(LbgPocApplication.class, args)));
	}

	@Override
	public void run(String... args) throws Exception {

		Object bean = context.getBean(args[0]);

		JobParametersBuilder parameters = new JobParametersBuilder();
		parameters.addDate("date", new SimpleDateFormat("yyyy-MM-dd").parse(args[1])).addString("path", args[2]);

		jobLauncher.run((Job) bean, parameters.toJobParameters());
	}

}
