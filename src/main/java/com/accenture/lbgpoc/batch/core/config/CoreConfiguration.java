package com.accenture.lbgpoc.batch.core.config;

import org.modelmapper.ModelMapper;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CoreConfiguration {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
	public static <T> LineMapper<T> lineMapper(String delimiter, String[] fieldNames, int[] fieldIndex, Class<T> type) {
		DefaultLineMapper<T> lineMapper = new DefaultLineMapper<>();

		// DelimitedLineTokenizer defaults to comma as its delimiter
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer(delimiter);
		lineTokenizer.setNames(fieldNames);
		lineTokenizer.setIncludedFields(fieldIndex);

		BeanWrapperFieldSetMapper<T> fieldSetMapper = new BeanWrapperFieldSetMapper<T>();
		fieldSetMapper.setTargetType(type);
		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		return lineMapper;
	}
	
	public static <T> LineAggregator<T> lineAggregator(String delimiter, String[] fieldNames, Class<T> type) {
		DelimitedLineAggregator<T> lineAggregator = new DelimitedLineAggregator<>();

		BeanWrapperFieldExtractor<T> fieldExtractor = new BeanWrapperFieldExtractor<T>();
		fieldExtractor.setNames(fieldNames);
		
		lineAggregator.setDelimiter(delimiter);
		lineAggregator.setFieldExtractor(fieldExtractor);
		return lineAggregator;
	}
}
