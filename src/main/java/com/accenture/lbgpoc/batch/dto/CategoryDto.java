package com.accenture.lbgpoc.batch.dto;

import lombok.Data;

@Data
public class CategoryDto {

	private Long id;
	
	private Long userId;
	
	private String category;
}
