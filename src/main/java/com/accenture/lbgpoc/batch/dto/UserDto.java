package com.accenture.lbgpoc.batch.dto;

import lombok.Data;

@Data
public class UserDto {

	private Long id;

	private String firstName;

	private String lastName;
	
	private CategoryDto category;
}
