package com.accenture.lbgpoc.batch.dto.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.accenture.lbgpoc.batch.dto.CategoryDto;
import com.accenture.lbgpoc.batch.dto.UserDto;

public class UserMapper implements RowMapper<UserDto>{

	@Override
	public UserDto mapRow(ResultSet resultSet, int lineNumber) throws SQLException {
		CategoryDto category = new CategoryDto();
		category.setCategory(resultSet.getString("CATEGORY"));

		UserDto user = new UserDto();
		user.setId(resultSet.getLong("ID"));
		user.setFirstName(resultSet.getString("FIRST_NAME"));
		user.setLastName(resultSet.getString("LAST_NAME"));
		user.setCategory(category);
		
		return user;
	}

}
