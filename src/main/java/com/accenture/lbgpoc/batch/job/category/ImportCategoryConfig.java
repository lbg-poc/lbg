package com.accenture.lbgpoc.batch.job.category;

import static com.accenture.lbgpoc.batch.core.config.CoreConfiguration.lineMapper;

import java.net.MalformedURLException;

import org.modelmapper.ModelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

import com.accenture.lbgpoc.batch.dto.CategoryDto;
import com.accenture.lbgpoc.batch.entity.CategoryEntity;
import com.accenture.lbgpoc.batch.repository.CategoryRepository;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class ImportCategoryConfig {

	public static final String JOB_NAME = "importCategoryJob";
	private static final String STEP_NAME = "importCategoryMainStep";
	private static final String READER_NAME = "importCategoryReader";
	private static final String DELIMITER = ",";
	private static final String[] FIELD_NAMES = new String[] { "id", "userId", "category" };
	private static final int[] FIELD_INDEX = new int[] { 0, 1, 2 };

	private final StepBuilderFactory stepBuilder;

	private final JobBuilderFactory jobBuilder;

	private final CategoryRepository categoryRepository;

	private final ModelMapper modelMapper;

	@Bean
	@StepScope
	public FlatFileItemReader<CategoryDto> importCategoryStepReader(@Value("#{jobParameters[path]}") String path)
			throws MalformedURLException {
		return new FlatFileItemReaderBuilder<CategoryDto>().name(READER_NAME).resource(new PathResource(path))
				.lineMapper(lineMapper(DELIMITER, FIELD_NAMES, FIELD_INDEX, CategoryDto.class)).build();
	}

	private ItemProcessor<CategoryDto, CategoryEntity> importCategoryStepProcessor() {
		return i -> modelMapper.map(i, CategoryEntity.class);
	}

	private ItemWriter<CategoryEntity> importCategoryStepWriter() {
		return i -> categoryRepository.saveAll(i);
	}

	private Step importCategoryStep() throws MalformedURLException {
		return stepBuilder.get(STEP_NAME).<CategoryDto, CategoryEntity>chunk(10).reader(importCategoryStepReader(null))
				.processor(importCategoryStepProcessor()).writer(importCategoryStepWriter()).allowStartIfComplete(true)
				.build();
	}

	@Bean(name = JOB_NAME)
	public Job importCategory() throws MalformedURLException {
		return jobBuilder.get(JOB_NAME).incrementer(new RunIdIncrementer()).start(importCategoryStep()).build();
	}
}