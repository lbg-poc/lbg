package com.accenture.lbgpoc.batch.job.user;

import static com.accenture.lbgpoc.batch.core.config.CoreConfiguration.lineAggregator;

import java.net.MalformedURLException;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

import com.accenture.lbgpoc.batch.dto.UserDto;
import com.accenture.lbgpoc.batch.dto.mapper.UserMapper;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class ExportQueryStep {

	private static final String READER_NAME = "exportReader";
	private static final String JOB_NAME = "exportJob";
	private static final String STEP_NAME = "exportMainStep";
	private static final String WRITER_NAME = "exportWriter";
	private static final String DELIMITER = ",";
	private static final String[] FIELD_NAMES = new String[] { "id", "firstName", "lastName", "category" };

	private static final String QUERY = "SELECT u.id, u.first_name, u.last_name, c.category "
			+ "FROM user u INNER JOIN category c ON u.id = c.user_id";

	private final StepBuilderFactory stepBuilder;

	private final JobBuilderFactory jobBuilder;

	private final DataSource dataSource;

	public ItemReader<UserDto> exportStepReader() {
		return new JdbcCursorItemReaderBuilder<UserDto>().name(READER_NAME).dataSource(dataSource).sql(QUERY)
				.rowMapper(new UserMapper()).build();
	}

	@Bean
	@StepScope
	public FlatFileItemWriter<UserDto> exportStepWriter(@Value("#{jobParameters[path]}") String path)
			throws MalformedURLException {
		return new FlatFileItemWriterBuilder<UserDto>().name(WRITER_NAME).resource(new PathResource(path)).append(true)
				.lineAggregator(lineAggregator(DELIMITER, FIELD_NAMES, UserDto.class)).build();
	}

	public Step exportStep() throws MalformedURLException {
		return stepBuilder.get(STEP_NAME).<UserDto, UserDto>chunk(10).reader(exportStepReader())
				.writer(exportStepWriter(null)).allowStartIfComplete(true).build();
	}

	@Bean(name = JOB_NAME)
	public Job importCategory() throws MalformedURLException {
		return jobBuilder.get(JOB_NAME).incrementer(new RunIdIncrementer()).start(exportStep()).build();
	}
}
