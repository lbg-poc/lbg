package com.accenture.lbgpoc.batch.job.user;

import static com.accenture.lbgpoc.batch.core.config.CoreConfiguration.lineMapper;

import java.net.MalformedURLException;

import org.modelmapper.ModelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

import com.accenture.lbgpoc.batch.dto.UserDto;
import com.accenture.lbgpoc.batch.entity.UserEntity;
import com.accenture.lbgpoc.batch.repository.UserRepository;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class ImportUserStep {

	private static final String JOB_NAME = "importUserJob";
	private static final String STEP_NAME = "importUserMainStep";
	private static final String READER_NAME = "importUserReader";
	private static final String DELIMITER = ",";
	private static final String[] FIELD_NAMES = new String[] { "id", "firstName", "lastName" };
	private static final int[] FIELD_INDEX = new int[] { 0, 1, 2 };

	private final StepBuilderFactory stepBuilder;

	private final JobBuilderFactory jobBuilder;

	private final UserRepository userRepository;

	private final ModelMapper modelMapper;

	@Bean
	@StepScope
	public FlatFileItemReader<UserDto> importUserStepReader(@Value("#{jobParameters[path]}") String path)
			throws MalformedURLException {
		return new FlatFileItemReaderBuilder<UserDto>().name(READER_NAME).resource(new PathResource(path))
				.linesToSkip(1).lineMapper(lineMapper(DELIMITER, FIELD_NAMES, FIELD_INDEX, UserDto.class)).build();
	}

	private ItemProcessor<UserDto, UserEntity> importUserStepProcessor() {
		return i -> modelMapper.map(i, UserEntity.class);
	}

	private ItemWriter<UserEntity> importUserStepWriter() {
		return i -> userRepository.saveAll(i);
	}

	public Step importUserStep() throws MalformedURLException {
		return stepBuilder.get(STEP_NAME).<UserDto, UserEntity>chunk(10).reader(importUserStepReader(null))
				.processor(importUserStepProcessor()).writer(importUserStepWriter()).allowStartIfComplete(true).build();
	}

	@Bean(name = JOB_NAME)
	public Job importCategory() throws MalformedURLException {
		return jobBuilder.get(JOB_NAME).incrementer(new RunIdIncrementer()).start(importUserStep()).build();
	}
}