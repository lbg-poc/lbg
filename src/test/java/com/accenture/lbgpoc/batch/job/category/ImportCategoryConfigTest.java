package com.accenture.lbgpoc.batch.job.category;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import com.accenture.lbgpoc.BatchTestConfiguration;
import com.accenture.lbgpoc.batch.core.config.CoreConfiguration;
import com.accenture.lbgpoc.batch.entity.CategoryEntity;
import com.accenture.lbgpoc.batch.repository.CategoryRepository;

@SpringBootTest
@EnableAutoConfiguration
@EnableBatchProcessing
@ContextConfiguration(classes = {ImportCategoryConfig.class, CoreConfiguration.class, BatchTestConfiguration.class})
@EntityScan(basePackageClasses = {CategoryEntity.class})
@EnableJpaRepositories(basePackageClasses = {CategoryRepository.class})
public class ImportCategoryConfigTest {

	private static final String TEST_INPUT = "category.csv";

	@Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;
	
	@Autowired
	private CategoryRepository categoryRepository;

	private JobParameters jobParameters() {
		JobParametersBuilder parameters = new JobParametersBuilder();
		parameters.addString("path", TEST_INPUT);
		return parameters.toJobParameters();
	}
	
	@Test
	void testImportCategory() throws Exception {
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParameters());
		JobInstance actualJobInstance = jobExecution.getJobInstance();
		ExitStatus actualJobExitStatus = jobExecution.getExitStatus();

		assertEquals(ImportCategoryConfig.JOB_NAME, actualJobInstance.getJobName());
		assertEquals("COMPLETED", actualJobExitStatus.getExitCode());
		
		assertEquals(4, categoryRepository.count());
	}

}
